package smell;

public class RowOrder {

	private final Item _item;
	private final int _quantity;

	public RowOrder(Item item, int quantity) {
		_item = item;
		_quantity = quantity;
	}

	public int getPrice() {
		return _quantity * _item.getPrice();
	}

}
