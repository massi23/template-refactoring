package smell;

import static org.junit.Assert.assertEquals;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Vector;

import org.junit.Test;

public class OrderTest {

	@Test
	public void getTotalOnEmptyOrder() {
		Order order = new Order();
		assertEquals(0, order.getTotal());
	}
	
	@Test
	public void getTotal() {
		Item redBook = new Item(0, 0, 100);
		Item greenBook = new Item(0, 0, 300);
		
		Order order = new Order();
		order.add(redBook, 10);
		order.add(greenBook, 3);
		assertEquals(1900, order.getTotal());
	}
	
	@Test
	public void writeOrderOnEmptyOrder() {
		Order order = new Order();
		
		StringWriter out = new StringWriter();
		PrintWriter pw = new PrintWriter(out);
		order.writeOrder(order, pw);
		pw.flush();
		pw.close();
		
		assertEquals("Order total = 0\n", out.toString());
	}

	@Test
	public void writeOrder() {
		Item redBook = new Item(0, 0, 10);
		redBook.setUnitPrice(100);
		Item greenBook = new Item(0, 0, 3);
		greenBook.setUnitPrice(300);
		
		Vector<Item> lineItems = new Vector<Item>();
		lineItems.add(redBook);
		lineItems.add(greenBook);
		ItemList items = new ItemList();
		items.setLineItems(lineItems);
		Order order = new Order(items);
		assertEquals(1900, order.getTotal());
		
		StringWriter out = new StringWriter();
		PrintWriter pw = new PrintWriter(out);
		order.writeOrder(order, pw);
		pw.flush();
		pw.close();
		
		assertEquals("Begin Line Item\n"+
						 "Product = 0\n"+
						 "Image = 0\n"+
						 "Quantity = 10\n"+
						 "Total = 1000\n"+
						 "End Line Item\n"+
						 "Begin Line Item\n"+
						 "Product = 0\n"+
						 "Image = 0\n"+
						 "Quantity = 3\n"+
						 "Total = 900\n"+
						 "End Line Item\n"+
						 "Order total = 1900\n", out.toString());
	}

}
