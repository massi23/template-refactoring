package smell;

import java.util.Vector;
 
class Item {
    protected int productId;
    private int ImageID;
    private int qty;
    private int _price;
	private Vector LineItems;
 
    public Item(int prodID, int ImageID, int price) {
        productId = prodID;
        this.ImageID = ImageID;
		_price = price;
    }
 
    int getProductID() {
        return productId;
    }
 
    int getImageID() {
        return ImageID;
    }
 
    int getQuantity() {
        return qty;
    }
 
    int getPrice() {
        return _price;
    }
 
    public void setProductID(int id) {
        productId = id;
    }
 
    public void setImageID(int ID) {
        ImageID = ID;
    }
 
    public void setQty(int qty) {
        this.qty = qty;
    }
 
    public void setUnitPrice(int i) {
        _price = i;
    }
}
